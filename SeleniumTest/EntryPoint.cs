﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Threading;

namespace SeleniumTest
{
    class EntryPoint
    {
        static void Main()
        {
            IWebDriver driver = new ChromeDriver();
            driver.Navigate().GoToUrl("http://testing.todvachev.com/selectors/name/");

            IWebElement element = driver.FindElement(By.Name("myName"));
            if (element.Displayed)
            {
                System.Console.WriteLine("Yesy it's found right in the page");
            }
            else
            {
                System.Console.WriteLine("No such element found!!");
            }
            Thread.Sleep(3000);
            driver.Quit();
        }
    }
}
